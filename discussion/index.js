//1. Import/require http from node.js (http is what allows us to create a server)
let http = require('http');

//2. Declare a variable assigned with a number value. This will be the port to be used or listened to by the server.
let port = 8000;

//3. Use http to create a server and write a header, then return data (like 'Hello World' string)
http.createServer(function(request,response) {

// 4. we write the header for the response specified its content and status.
	response.writeHead(200, {'Content-Type': 'text/plain'});

// 5. ends the function and return a string with 'Hello World'
	response.end('Hello World');

//6. Attach a port to the server specifying where you can access the server.
}).listen(port)

//7. Let the terminal/cconsole know that the server is running on which port.
console.log(`Server is running at a localhost: ${port}`);